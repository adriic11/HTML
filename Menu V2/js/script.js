//Declarar variables
var sideMenu = document.getElementById('menu-side'),
    btnAbrir = document.getElementById('btn-abrir'),
    cuerpo = document.getElementById('cuerpo');


//Evento para mostrar y ocultar menú
function open_close_menu() {
    cuerpo.classList.toggle('body-move');
    sideMenu.classList.toggle('menu-side-move');
}

//Ejecutar evento click
btnAbrir.addEventListener('click', open_close_menu);

//Si el ancho de la página es menor a 760px, ocultará el menú al recargar la página
if (window.innerWidth < 760) {
    cuerpo.classList.add('body-move');
    sideMenu.classList.add('menu-side-move');
}

//Hacemos el menu responsive
window.addEventListener('resize', function () {
    if(window.innerWidth > 760) {
        cuerpo.classList.remove('body-move');
        sideMenu.classList.remove('menu-side-move');
    }
    if(window.innerWidth < 760) {
        cuerpo.classList.add('body-move');
        sideMenu.classList.add('menu-side-move');
    }
});
