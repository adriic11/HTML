const toggleMenu = document.getElementById('icon-menu'),
    btnResponsive = document.getElementById('icono-responsive'),
    lateralMenu = document.getElementById('menu-lateral'),
    containerMenu = document.getElementById('container-menu-lateral'),
    navHeader = document.getElementById('header-menu'),
    modalUser = document.getElementById('modal-user'),
    modalUserContainer = document.getElementById('modal-user-container'),
    btnUsuario = document.getElementById('button-usuario'),
    iconoUsuario = document.getElementById('icono-usuario'),
    btnAjustes = document.getElementById('button-ajustes'),
    iconoAjustes = document.getElementById('icono-ajustes'),
    modalAjustes = document.getElementById('modal-ajustes'),
    modalHeader = document.getElementById('modal-user-header'),
    cuerpo = document.getElementById('cuerpo');


btnResponsive.addEventListener('click', function() {
    if(navHeader.classList.contains("active"))
    {
        navHeader.classList.remove('active');
    } else {
        navHeader.classList.add('active');
    }
})

toggleMenu.addEventListener('click', ()=> {
   containerMenu.classList.toggle('container-menu-lateral--show');
   cuerpo.classList.toggle('body-move');
});

//Boton Usuario
btnUsuario.addEventListener('click', function() { 
    if (modalUser.classList.contains('active'))
        modalUser.classList.remove('active');
    else
        modalUser.classList.add('active');
});

btnAjustes.addEventListener('click', function() {
    if (modalAjustes.classList.contains('active'))
        modalAjustes.classList.remove('active');
    else
        modalAjustes.classList.add('active');
});

//Si el ancho de la página es menor a 760px, ocultará el menú al recargar la página
if (window.innerWidth < 760) {
    containerMenu.classList.add('container-menu-lateral--show');
    cuerpo.classList.add('body-move');
}

window.addEventListener('resize', function () {
    if(window.innerWidth > 760) {      
        containerMenu.classList.remove('container-menu-lateral--show');
        cuerpo.classList.remove('body-move');
    }
    if(window.innerWidth < 760) {
        containerMenu.classList.add('container-menu-lateral--show');
        cuerpo.classList.add('body-move');
    }
});

document.addEventListener("click", function(e){    
    //obtiendo informacion del DOM para  
    var clic = e.target;
   

   
    

}, false);