var btnAbrirPopup = document.getElementById('btn-abrir-popup'),
    btnCerrarPopup = document.getElementById('btn-cerrar-popup'),
    popup = document.getElementById('popup'),
    mensaje = document.getElementById('mensaje'),
    btnAbrirAdvert = document.getElementById('btn-abrir-advert'),
    overlay = document.getElementById('overlay');

btnAbrirPopup.addEventListener('click', function() { 
    icon.className ='fa-solid fa-circle-check fa-xl';
    mensaje.innerText = "Success";
    popup.style.backgroundColor = '#27AE60';
    popup.style.borderLeft = '5px solid #178344';

    overlay.classList.add('active');
    popup.classList.add('active');
    setTimeout(() => {
        popup.classList.remove('active');  
    }, 10000);
});

btnCerrarPopup.addEventListener('click', function() {
    overlay.classList.remove('active');
    popup.classList.remove('active');
    
});

btnAbrirAdvert.addEventListener('click', function() {
    icon.className ='fa-solid fa-circle-exclamation fa-xl';
    mensaje.innerText = "Blocked";
    popup.style.backgroundColor = '#F93636';
    popup.style.borderLeft = '5px solid #C0392B';

    overlay.classList.add('active');
    popup.classList.add('active');
    setTimeout(() => {
        popup.classList.remove('active');  
    }, 10000);
});

document.addEventListener("click", function(e){    
    //obtiendo informacion del DOM para  
    var clic = e.target;
    console.log(clic);
    if( clic != popup && clic != btnAbrirPopup && clic != btnAbrirAdvert){
     popup.classList.remove('active');
    }
}, false);