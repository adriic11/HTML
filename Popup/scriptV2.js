var btnAbrirPopup = document.getElementById('btn-abrir-popup'),
    btnCerrarPopup = document.getElementById('btn-cerrar-popup'),
    popup = document.getElementById('popup'),
    mensaje = document.getElementById('mensaje'),
    btnAbrirAdvert = document.getElementById('btn-abrir-advert'),
    overlay = document.getElementById('overlay');

btnAbrirPopup.addEventListener('click', function abrirPopup() {

    icon.className ='fa-regular fa-circle-check fa-xl';
    mensaje.innerText = "Succes";
   
});

btnCerrarPopup.addEventListener('click', function() {
    overlay.classList.remove('active');
    popup.classList.remove('active');
   
});

btnAbrirAdvert.addEventListener('click', function() {
    icon.className ='fa-solid fa-circle-exclamation fa-xl';
    mensaje.innerText = "Blocked";
});

document.addEventListener("click", function(e){    
    //obtiendo informacion del DOM para  
    var clic = e.target;
    console.log(clic);
    if( clic != popup && clic != btnAbrirPopup && clic != btnAbrirAdvert){
     popup.classList.remove('active');
    }
}, false);

function abrirPopup() {
    overlay.classList.add('active');
    popup.classList.add('active');
    setTimeout(() => {
        popup.classList.remove('active');  
    }, 10000);
}



@media (max-width:1400px) {
    .popup{
        width: calc(100% - 50px);
    }
}
@media (max-width:400px) {
    .popup{
        width: calc(100% - 50px);
    }
    
}